//  Created by Imran Peerbhai on 8/15/13.
//
//
// GPIO is a wrapper for BeagleBone to make BeagleBone GPIO somewhat like Arduino.
//  There are some huge assumptions I make in this concept.
// 1. The user knows how to calculate pins from the funky beaglebone Muxing scheme.
// 2. The user knows some things about arduino...

// License:
// 	You may use this file in your project in any way you like for non-commercial use.  I request that you attribute me, Imran Peerbhai,
//    in your application's credits.
//  Commercial use:
//		this file may be used commercially without monetary payment in exchange for the following considerations:
//			A. My name must be on the application in some method visible to the end user.
//			B. You must provide me a copy of your application's source code.
//			C. You must provide me with contact information to license any of your source code.
//
// Warrantee:
//		This code may not work at all.  USE AT YOUR OWN RISK.
// Imnendification:
//      By using this code, you agree to imdenify me of any risk from this code or derivatives.

#ifndef _GPIO_h
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#define _GPIO_h
#endif


//------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------
// Class definition.
//------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------

class WrappedGPIO
{
    //------------------------------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------------------
public:
    //------------------------------------------------------------------------------------------------------------------------------
    // Function:
    //  This function opens the BeagleBone export file in /sys/class/gpio, writes the pin to it, then closes the file.
    // Parameters:
    //  int nPin -- the GPIO pin number
    //  cont char * pExportFileName -- the name of the export file pere BBB reference doc.
    int exportHelper( int , const char * );

    //------------------------------------------------------------------------------------------------------------------------------
    //	Function:
    //		Asks the kernel to give me this pin in user mode.
    //	 Parameters:
    //		int nPin -- the pin number, in GPIO terms, to export.
    // Returns:
    //	 0 -- S_OK
    // <0 -- S_ERROR
    // >0 -- S_WARNING
    int exportPin( int );

    //------------------------------------------------------------------------------------------------------------------------------
    //	 Parameters:
    //		int nPin -- the pin number, in GPIO terms, to export.
    // Returns:
    //	 0 -- S_OK
    // <0 -- S_ERROR
    // >0 -- S_WARNING
    int unexport( int ); // will release the pin back to the kernel.
    
    //------------------------------------------------------------------------------------------------------------------------------
    //	 Parameters:
    //		int nPin -- the pin number, in GPIO terms, to export.
    //    int nDirection.  0 -- output, 1 -- input.
    // Returns:
    //	 0 -- S_OK
    // <0 -- S_ERROR
    // >0 -- S_WARNING
    int pinMode( int, int); // Will set either a read or write direction.
    
    //------------------------------------------------------------------------------------------------------------------------------
    //	 Parameters:
    //		int nPin -- the pin number, in GPIO terms, to export.
    //		int nValue -- the value, either 0 or 1.
    // Returns:
    //	 0 -- S_OK
    // <0 -- S_ERROR
    // >0 -- S_WARNING
    int digitalWrite( int, int ); // Will write "high" or low to a pin that's been exported.
    
    //------------------------------------------------------------------------------------------------------------------------------
    //	 Parameters:
    //		int nPin -- the pin number, in GPIO terms, to export.
    // Returns:
    //	 0 -- LOW
    // <0 -- S_ERROR
    // 1 -- HIGH
    int digitalRead( int ); // Will read the digital value from a pin that's been exported.
    
    //------------------------------------------------------------------------------------------------------------------------------
    // Function:
    //  streamWriter writes to a file as a stream.  It flushes the file handle after each write.  This is to use the BeagleBone
    //  file IO system to set pin levels
    // Parameters:
    //  FILE *pPinFile -- A file handle to an open GPIO pin.
    //  int nValue -- the value to write( 0 || 1 ).
    int streamWrite( FILE *, int );
    
    //------------------------------------------------------------------------------------------------------------------------------
    // Function:
    //  delayuS tries to sleep a thread x micro-seconds.  ( uS -- microseconds... ).
    // Parameters:
    //  int nuS -- the number of microseconds.
    void delayuS( int );
}; // end class
