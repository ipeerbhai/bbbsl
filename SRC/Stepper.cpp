//
//  Stepper.cpp
//  
//
//  Created by Imran Peerbhai on 8/29/13.
//
//

#include "Stepper.h"


Stepper::Stepper()
{
	m_bInited = false;
	m_nuSMax = 10000;
	m_nuSMin = 400;
	m_nStepAngle = 0.9;
	nusCurrent = m_nuSMax;
	m_nuSDiscreteAccel = 10;
	m_nuSTarget = 1000;
};


int Stepper::Step(int nSteps )
{
	//initialize a couple of file pointers for the streamwriter.
	if(!m_bInited)
	{
		int nStepPin = 44;
		int nDirPin = 45;

		// Export the pins.
		m_gpStep.exportPin(nStepPin);
		m_gpStep.pinMode(nStepPin, 0);
		m_gpDir.exportPin(nDirPin);
		m_gpDir.pinMode( nDirPin, 0 );

		// Create file pointers for the stream writer.
		char strGPIOStepValueFileName[50]; // A buffer to generate the "gpio" direction absolute file name.
		char strGPIODirValueFileName[50]; // A buffer to generate the "gpio" direction absolute file name.
		sprintf( strGPIOStepValueFileName, "/sys/class/gpio/gpio%d/value",  nStepPin );
		sprintf( strGPIODirValueFileName, "/sys/class/gpio/gpio%d/value",  nDirPin );

		// Open the file and write the digital value.
		m_pStepFile = fopen( strGPIOStepValueFileName, "w" ); // open the value file for write
		m_pDirFile = fopen( strGPIODirValueFileName, "w" ); // open the value file for write
		m_bInited = true;
	}
	for( int nCount = 0; nCount < nSteps; nCount++)
	{
		m_gpStep.streamWrite( m_pStepFile, 1);
		m_gpStep.delayuS( 2 );
		m_gpStep.streamWrite( m_pStepFile, 0);
	}
	return(0);
};

void Stepper::TurnMotor()
{
	using namespace std;
	WrappedGPIO gpWait;
	int nDelayuS = m_nuSMax; // default to 1000 uS... ( aka 1 millisecond. )
	while(m_bContinueRunning)
	{
		if ( nDelayuS > m_nuSTarget )
			nDelayuS -= m_nuSDiscreteAccel;
		Step(1);
		//cout << "sleeping: " << nDelayuS << endl;
		gpWait.delayuS( nDelayuS );

	}
}



int main(int argc, char *argv[])
{
	using namespace std;
	Stepper myStepper;
	if ( argc != 1 )
	{
		myStepper.m_nuSTarget = atoi(argv[1]); // Keep it 1000 uS.
		cout << "nDelayuS = " <<  myStepper.m_nuSTarget << endl;
	}
	printf("Starting \n");
	myStepper.TurnMotor();
	return(0); // we gotta get out of here...
}