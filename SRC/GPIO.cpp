#include "GPIO.h"
//------------------------------------------------------------------------------------------------------------------------------
int WrappedGPIO::exportHelper( int nPin, const char *pExportFileName )
{
	FILE *pExportFile = NULL; // Will use this pointer to get the handle from FOPEN for the hard-coded file path.

	// Open the file for write, and write the pin number to it.
	pExportFile = fopen( pExportFileName, "w" );
	if ( NULL == pExportFile )
	{
		// Uh -oh, we couldn't open the file for write.
		printf("Could not access export subsystem for pin %d.", nPin );
		return( -1 );
	}
	fprintf( pExportFile, "%d", nPin); // write the GPIO pin number to the export file so the kernel can export it.
	fclose( pExportFile ); // It's done!
	return(0); // all good.
}

//------------------------------------------------------------------------------------------------------------------------------
int WrappedGPIO::exportPin( int nPin )
{
	FILE *pExportFile = NULL; // Will use this pointer to get the handle from FOPEN for the hard-coded file path.
	const char *pExportFileName = "/sys/class/gpio/export"; // Per BBB reference.
	return( exportHelper( nPin,  pExportFileName) );
}

//------------------------------------------------------------------------------------------------------------------------------
int WrappedGPIO::unexport( int nPin )
{
	FILE *pExportFile = NULL; // Will use this pointer to get the handle from FOPEN for the hard-coded file path.
	const char *pExportFileName = "/sys/class/gpio/unexport"; // Per BBB reference.
	return( exportHelper( nPin,  pExportFileName) );
}

//------------------------------------------------------------------------------------------------------------------------------
int WrappedGPIO::pinMode( int nPin, int nPinMode )
{
	FILE *pDirectionFile = NULL; // The direction file for the pin.
	char strGPIODirFileName[50]; // A buffer to generate the "gpio" direction absolute file name.
	sprintf( strGPIODirFileName, "/sys/class/gpio/gpio%d/direction",  nPin ); // write out the concatenated file name.

	pDirectionFile = fopen( strGPIODirFileName, "w" ); // Open the direction file for writing.
	if ( NULL == pDirectionFile )
	{
		// Uh-oh, can't write the file...
		printf( "Could not open %s for write", strGPIODirFileName);
		return( -1 );
	}
	// check to see the direction and write out the correct value
	if ( nPinMode == 0 )
	{
		// We're in "output" mode.
		fprintf( pDirectionFile, "out" );
	}
	else
	{
		// We're in the "in" mode.
		fprintf( pDirectionFile, "in" );
	}
	fclose( pDirectionFile );
	return ( 0 );
	
}
//------------------------------------------------------------------------------------------------------------------------------
int WrappedGPIO::digitalWrite( int nPin, int nValue )
{
	FILE *pValueFile = NULL; // The direction file for the pin.
	char strGPIOValueFileName[50]; // A buffer to generate the "gpio" direction absolute file name.
	sprintf( strGPIOValueFileName, "/sys/class/gpio/gpio%d/value",  nPin );
	
	// Open the file and write the digital value.
	pValueFile = fopen( strGPIOValueFileName, "w" ); // open the value file for write
	if ( NULL == pValueFile )
	{
		// Uh-oh, couldn't open the file to write.
		printf( "could not open %s for write.", strGPIOValueFileName);
		return( -1 );
	}
	fprintf( pValueFile, "%d",  nValue);
	fclose( pValueFile );
	return ( 0 );
}
//------------------------------------------------------------------------------------------------------------------------------
int WrappedGPIO::streamWrite( FILE *pValueFile, int nValue )
{
	if ( NULL == pValueFile )
	{
		// Uh-oh, invalid file handle.
		printf( "streamWrite -- Invalid File handle, aborting. \n");
		return( -1 );
	}
	fprintf( pValueFile, "%d",  nValue);
	fflush( pValueFile );
	return ( 0 );
}

//------------------------------------------------------------------------------------------------------------------------------
int WrappedGPIO::digitalRead( int nPin )
{
	FILE *pValueFile = NULL; // The direction file for the pin.
	int nReadValue = -1; // will hold the read value.
	char strGPIOValueFileName[50]; // A buffer to generate the "gpio" direction absolute file name.
	sprintf( strGPIOValueFileName, "/sys/class/gpio/gpio%d/value",  nPin );
	
	// Open the file and write the digital value.
	pValueFile = fopen( strGPIOValueFileName, "r" ); // open the value file for write
	if ( NULL == pValueFile )
	{
		// Uh-oh, couldn't open the file to write.
		printf( "could not open %s for write.", strGPIOValueFileName);
		return( -1 );
	}
	nReadValue = fgetc( pValueFile );
	fclose( pValueFile );
	return ( nReadValue );
}

//------------------------------------------------------------------------------------------------------------------------------
void WrappedGPIO::delayuS( int nuSToDelay )
{
	struct timespec req = {0};
	req.tv_sec = 0;
	req.tv_nsec = nuSToDelay * 1000L; // convert microseconds to nanoseconds
	nanosleep(&req, (struct timespec *)NULL);
}
/*
//------------------------------------------------------------------------------------------------------------------------------
int main(int argc, char **argv)
{
	// "Setup" area.
	// This is a test now...
	int nPin = 44;
	printf( "Entering loop \n");
//	const int nShortPulse = 1000; // 1000 uS or 1 ms.
//	const int nLongPulse = 2000; // 2000 us or 2 ms.
	const int nInterval = 25000; // 25000 uS or 25ms
	
	int nShortPulse; // The length of the short puse in uS.
	int nLongPulse; // the length in uS of the long pulse.
	int nSleepAtEnd; // The number of seconds to sleep between cycles.
	 if ( argc < 4 )
	 {
	 	printf( "invalid arguments.  must be shortInterval, longIngterval, delay\n" );
	 	return( -1 );
	 }
	 
	 nShortPulse = atoi(argv[1]);
	 nLongPulse = atoi(argv[2]);
	 nSleepAtEnd = atoi(argv[3]);
	
	
	// decae objects, begin building out my delay timers.

	WrappedGPIO myGPIO;
	myGPIO.exportPin( 44 ); // prep pin 12 on p8 for write
	myGPIO.pinMode( 44, 0 ); // set it to output mode.
	FILE *pValueFile = NULL; // The direction file for the pin.
	char strGPIOValueFileName[50]; // A buffer to generate the "gpio" direction absolute file name.
	sprintf( strGPIOValueFileName, "/sys/class/gpio/gpio%d/value",  nPin );
	
	// Open the file and write the digital value.
	pValueFile = fopen( strGPIOValueFileName, "w" ); // open the value file for write

	// "loop" area.
	while( 1 )
	{
		// simulate PCM with a lot of jitter...
		printf( "begin cycle with %d uS Pulse \n", nLongPulse);
		for( int nCount = 0; nCount < 50; nCount++ )
		{
			myGPIO.streamWrite( pValueFile, 1 );
			delayuS(nLongPulse);
			myGPIO.streamWrite( pValueFile, 0);
			delayuS(20000);
		}
			
		printf( "sleeping %d uS \n", nInterval);
		delayuS(nInterval);
		printf( "%d uS Pulse \n", nShortPulse);
		for( int nCount = 0; nCount < 50; nCount++ )
		{
			myGPIO.streamWrite( pValueFile, 1 );
			delayuS(nShortPulse);
			myGPIO.streamWrite( pValueFile, 0);
			delayuS(20000);
		}

		printf( "End cycle by sleeping %d S \n", nSleepAtEnd);
		sleep(nSleepAtEnd);
	}
	fclose(pValueFile);
	return(0);
}
 */

