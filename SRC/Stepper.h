//
//  Stepper.h
//  
//
//  Created by Imran Peerbhai on 8/29/13.
//
//

#ifndef ____Stepper__
#define ____Stepper__

#include <iostream>
#include <pthread.h>
#include <cmath>

#endif /* defined(____Stepper__) */

#ifndef _GPIO_h
#include "GPIO.h"
#endif

class Stepper
{
private:
	WrappedGPIO m_gpStep; // used for step
	WrappedGPIO m_gpDir; // used for direction.
	FILE *m_pStepFile; // Used for the GPIO StreamWriter, step pin.
	FILE *m_pDirFile; // Used for the GPIO StreamWriter, Dir pin.
	bool m_bInited;
	int m_nuSMax; // The max number of uS before the motor is essentially stopped.
	int m_nuSMin; // The min number of uS
	bool m_bContinueRunning; // whether we keep running or not.
	float m_nStepAngle; // The degrees of the motor of a full step.
	int nusCurrent; // The current uS wait between steps. 


public:
	int m_nuSDiscreteAccel; // How many uS to remove from the current until we hit our target.
	int m_nuSTarget; // What to hit when we want to accelerate to target.
	Stepper();
	int Step(int);
	void TurnMotor(); // Turn the motor until we should stop by something changing my m_bContinuerunning variable.
};