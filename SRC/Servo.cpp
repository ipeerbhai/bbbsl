//
//  Servo.cpp
//  
//
//  Created by Imran Peerbhai on 8/15/13.
//
//
// Copyright 2013, Imran Peerbhai, All rights reserved.

#include "Servo.h"

Servo::Servo(int nPin)
{
    m_nPin = nPin;
    myGPIO.export(nPin);
    myGPIO.pinMode(nPin, 0);
    char strGPIOValueFileName[50]; // A buffer to generate the "gpio" direction absolute file name.
	sprintf( strGPIOValueFileName, "/sys/class/gpio/gpio%d/value",  nPin );
	
	// Open the file and write the digital value.
	m_pValueFile = fopen( strGPIOValueFileName, "w" ); // open the value file for write

}

Servo::Servo()
{
    Servo(44);
}

Servo::~Servo()
{
    if ( NuLL != m_pValueFile)
    {
        fclose(m_pValueFile);
    }
    myGPIO.unexport(m_nPin);
}


int Servo::write( float nAngle )
{
    // We need to convert an angle into a uS signal.
    int nuS = 600; // How many uS to pulse.  600 = 0 degrees.
    int nDecaAngle = floor( nAngle * 10 );
    nuS = nDecaAngle + 600; // The 600 is kind of a wild guess.  It's somewhere between 542 and 600, depending on servo motor.
    if ( 2400 > nuS )
    {
        // That's beyond the servo spec -- set it to max signal.
        nuS = 2400;
    }
    
    // Begin a write cycle, and keep writing ever 20ms.
    while ( m_bRefresh )
    {
        myGPIO.streamWrite( m_pValueFile, 1);
        myGPIO.delayuS( nuS );
        myGPIO.streamWrite( m_pValueFile, 0);
        myGPIO.delayuS( 20000 );
        
    }
    return(0);
}

int min( int argc, char **argv)
{
    return(0);
}