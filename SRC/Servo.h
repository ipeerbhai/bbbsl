//
//  Servo.h
//  
//
//  Created by Imran Peerbhai on 8/15/13.
//
//

#ifndef ____Servo__
#define ____Servo__
#include <iostream>
#include <pthread.h>
#include <cmath>

#endif /* defined(____Servo__) */

#ifndef _GPIO_h
#include "GPIO.h"
#endif

class Servo
{
private:
    WrappedGPIO myGPIO; // Will use this to actually do exports and the like.
    int m_nPin; // the pin this class used.
    bool m_bRefresh = FALSE;
    FILE *m_pValueFile = NULL; // The direction file for the pin.

public:
    Servo(int); // over-ride the default constructor.
    Servo();
    ~Servo();
    int write(float);
};